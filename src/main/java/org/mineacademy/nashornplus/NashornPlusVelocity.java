package org.mineacademy.nashornplus;

import java.util.logging.Logger;

import com.google.inject.Inject;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;

@Plugin(id = "nashornplus", name = "NashornPlus", version = "1.0.0", url = "https://mineacademy.org", authors = { "kangarko" })
public class NashornPlusVelocity {

	//private final ProxyServer server;
	//private final Logger logger;

	@Inject
	public NashornPlusVelocity(ProxyServer server, Logger logger) {
		//this.server = server;
		//this.logger = logger;

		final javax.script.ScriptEngineManager engineManager = new javax.script.ScriptEngineManager();
		final javax.script.ScriptEngineFactory engineFactory = new org.openjdk.nashorn.api.scripting.NashornScriptEngineFactory();

		// Register Nashorn library
		engineManager.registerEngineName("Nashorn", engineFactory);
	}
}