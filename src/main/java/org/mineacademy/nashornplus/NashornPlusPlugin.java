package org.mineacademy.nashornplus;

import org.bukkit.plugin.java.JavaPlugin;

public final class NashornPlusPlugin extends JavaPlugin {

	@Override
	public void onEnable() {

		final javax.script.ScriptEngineManager engineManager = new javax.script.ScriptEngineManager();
		final javax.script.ScriptEngineFactory engineFactory = new org.openjdk.nashorn.api.scripting.NashornScriptEngineFactory();

		// Register Nashorn library
		engineManager.registerEngineName("Nashorn", engineFactory);

		//
		// Example usage
		//

		/*final javax.script.ScriptEngine engine = engineManager.getEngineByName("Nashorn");
		
		// Run JavaScript code as usual
		try {
			engine.eval("print('Hello World!');");
		
		} catch (final javax.script.ScriptException ex) {
			ex.printStackTrace();
		}*/
	}
}
