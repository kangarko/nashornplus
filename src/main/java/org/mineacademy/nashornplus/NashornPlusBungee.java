package org.mineacademy.nashornplus;

import net.md_5.bungee.api.plugin.Plugin;

public final class NashornPlusBungee extends Plugin {

	@Override
	public void onEnable() {

		final javax.script.ScriptEngineManager engineManager = new javax.script.ScriptEngineManager();
		final javax.script.ScriptEngineFactory engineFactory = new org.openjdk.nashorn.api.scripting.NashornScriptEngineFactory();

		// Register Nashorn library
		engineManager.registerEngineName("Nashorn", engineFactory);

		//
		// Example usage
		//

		/*final javax.script.ScriptEngine engine = engineManager.getEngineByName("Nashorn");

		// Run JavaScript code as usual
		try {
			engine.eval("print('Hello World!');");

		} catch (final javax.script.ScriptException ex) {
			ex.printStackTrace();
		}*/
	}
}
